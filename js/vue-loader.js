(function () {
    if (!window.jQuery) { // 检测是否加载Jquery
        console.info('need jquery required.');
    } else if (!window.Vue) { // 检测是否加载Vue
        console.info('need Vue required.');
    } else {
        var loader = {
            load: function (template_link, options, resolve, reject) {
                $.ajax({
                        url: template_link,
                        type: 'GET',
                        dataType: 'html',
                        timeout: 5000
                    })
                    .done(function (data) {
                        resolve($.extend({
                            template: data
                        }, options));
                    })
                    .error(function (err) {
                        reject(err.toString());
                    });
            },
            install: function (vue) {
                var self = this;
                vue.createLazyComponent = function (id, templateLink, options) {
                    var load_func = self.load.bind(self, templateLink, options);
                    return vue.component(id, load_func);
                }
            }
        };
        Vue.use(loader);
    }
})();