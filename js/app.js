var routes = [{
    name: 'news',
    path: '/news',
    component: newsController
}, {
    name: 'search',
    path: '/search',
    component: searchController
}, {
    name: 'my',
    path: '/my',
    component: myController
}, {
    path: '/',
    redirect: '/news'
}];
var router = new VueRouter({
    routes: routes,
    linkActiveClass: 'selected'
});
var app = new Vue({
    el: '#app',
    router: router,
    data: function () {
        return {
            tabs: [{
                    id: 'news',
                    title: '最新',
                    to: {
                        name: 'news'
                    }
                },
                {
                    id: 'search',
                    title: '搜索',
                    to: {
                        name: 'search'
                    }
                },
                {
                    id: 'my',
                    title: '我的',
                    to: {
                        name: 'my'
                    }
                }
            ]
        };
    }
});