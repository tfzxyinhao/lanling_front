var newsController = Vue.createLazyComponent('news', 'views/news.html', {
    data: function () {
        return {
            info:'关注公众号,及时收到推送',
            works:[
                
            ]
        }
    },
    mounted: function(){
        this.loadWorks();
    },
    methods: {
        loadWorks: function(){
            for(var i =0;i<10;i++){
                this.works.push({
                    id: 'id1',
                    logo:'https://www.lgstatic.com/i/image/M00/08/D2/Cgp3O1bRFcSAHMtPAAAelCJqdO8411.png',
                    name: '电气技术员',
                    company:'新中源集团',
                    city: '高安',
                    money: '4000-4500元',
                    time : '今天 18:47'
                });
            }
        }
    }
});

var searchController = Vue.createLazyComponent('search', 'views/search.html', {
    data: function () {
        return {
            works:[
                
            ]
        }
    },
    methods: function () {

    }
});

var myController = Vue.createLazyComponent('my', 'views/my.html', {
    data: function () {
        return {
            avatar:'https://www.lgstatic.com/images/myresume/default_headpic.png'
        }
    },
    methods: function () {

    }
});

var loginController = Vue.createLazyComponent('login', 'views/login.html', {
    data: function () {
        return {}
    },
    methods: function () {

    }
});