var api_host = 'http://lanlingweilai.work/api'
// http 请求超时 5s
var request_timeout = 5000;

// session过期时间72小时
var session_timeout = 72 * 3600 * 1000;

function parse(query) {
    var params = {};
    var arr = query.match(/([^=&]+)=([^&]+)/ig);
    for (var i in arr) {
        var items = arr[i].split('=');
        if (items && items.length == 2) {
            params[items[0]] = items[1];
        }
    }
    return params;
}

function parseSearch() {
    return parse(window.location.search.slice(1));
}

function parseHash() {
    var arr = window.location.hash.split("?");
    if (arr.length == 2) {
        var params = parse(arr[1]);
        params.fromApp = true;
        return params;
    } else {
        return {
            fromApp: false
        };
    }
}

function parseParams() {
    var params = {};
    var params1 = parseHash();
    var params2 = parseSearch();
    for (var k in params1) {
        params[k] = params1[k];
    }
    for (var k in params2) {
        params[k] = params2[k];
    }
    return params;
}

var init_params = parseParams();
init_params.debug = 1;

/**
 * mongodb日期转string格式日期
 * @param {string} d 
 */
function date_pretty(d) {
    return d.split('.')[0].replace('T', ' ');
}

/**
 * mongodb日期转string格式日期
 * @param {string} d 
 */
function date_str(d) {
    return d.split('T')[0];
}

/**
 * mongodb日期转JS日期
 * @param {string} d 
 */
function date_parse(d) {
    arr = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/g.exec(d);
    if (arr && arr.length == 7) {
        return new Date(parseInt(arr[1], 10), parseInt(arr[2], 10) - 1, parseInt(arr[3], 10), parseInt(arr[4], 10), parseInt(arr[5], 10), parseInt(arr[6], 10));
    }
    return new Date(Date.parse(d));
}

/**
 * mongodb日期提取年月日
 * @param {string} d 
 */
function get_date(d) {
    var day = d.getDate();
    var month = d.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    if (day < 10) {
        day = '0' + day;
    }
    return [d.getFullYear(), month, day].join('-');
}

// 手机号码隐藏
function phone_pretty(phone) {
    return phone.replace(/^(\d{4})\d+(\d{3})$/, '$1****$2');
}

// 判断是否是微信
function isWeixinBrowser() {
    return /micromessenger/.test(navigator.userAgent.toLowerCase())
}

// 是否是调试模式
function isDebug() {
    return init_params.debug == 1;
}

/**
 * ajax请求
 * @param {string} url - 请求地址
 * @param {object} data - 参数
 * @param {string} method - 请求方式
 * @param {Function} cb - 回调
 */
function http_json_request(url, data, method, cb) {
    data.sessionToken = sessionToken;
    var callback = cb || function () {};
    if (method == 'DELETE') {
        url = url + '?' + $.param(data);
    } else if (method != 'GET') {
        data = JSON.stringify(data);
    }
    $.ajax({
            url: url,
            data: data,
            type: method,
            dataType: 'json',
            timeout: request_timeout,
            contentType: 'application/json'
        })
        .done(function (data) {
            if (data.code && data.code == 401) {
                window.location.href = url_login;
            } else {
                callback(true, data);
            }
        })
        .error(function (err) {
            callback(false);
        });
}

/**
 * Get请求,获取数据时使用
 * @param {string} url - 请求地址
 * @param {object} data - 参数
 * @param {Function} cb - 回调
 */
function http_get(url, data, cb) {
    http_json_request(url, data, 'GET', cb);
}

/**
 * Post请求,新建数据时使用
 * @param {string} url - 请求地址
 * @param {object} data - 参数
 * @param {Function} cb - 回调
 */
function http_post(url, data, cb) {
    http_json_request(url, data, 'POST', cb);
}

/**
 * Patch请求,修改数据时使用
 * @param {string} url - 请求地址
 * @param {object} data - 参数
 * @param {Function} cb - 回调
 */
function http_patch(url, data, cb) {
    http_json_request(url, data, 'PATCH', cb);
}

/**
 * Delete请求,删除数据时使用
 * @param {string} url - 请求地址
 * @param {object} data - 参数
 * @param {Function} cb - 回调
 */
function http_delete(url, data, cb) {
    http_json_request(url, data, 'DELETE', cb);
}


/**
 * 创建随机字符串
 */
function makeRandomStr() {
    var str = '';
    for (var i = 0; i < 5; i++) {
        str += alpha[Math.floor(Math.random() * 26)];
    }
    return str;
}

/**
 * 生成唯一用户Id
 */
function makesure_uid() {
    var uid = Cookies.get('uid');
    if (!uid) {
        var uid = 'da_' + Date.now().toString() + '_' + makeRandomStr();
        Cookies.set('uid', uid);
        return uid;
    }
    return uid;
}


var basic_param = {
    udid: makesure_uid(),
    timestamp: Math.ceil(Date.now() / 1000).toString(),
    appId: 'NurseNote@HY',
    platform: clientInfo.engine.name,
    os: clientInfo.os.name + ' ' + clientInfo.os.version,
    osType: clientInfo.os.name,
    bundleId: 'com.nursenote.pc'
};